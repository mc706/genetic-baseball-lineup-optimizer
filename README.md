# Baseball Lineup Genetic Optimizer

This project is an exploration of a problem space and a technology space. 
The problem space is trying to answer the question "what is the best lineup
of a given team of baseball players". The technology space is the world of 
Web Assembly, Web Workers, Dynamic Programming and Genetic Algorithms. 

## The Problem
Baseball is a sport with many statistics and millions of variables. One of the 
many decisions a coach needs to make is what order do you put players in the 
lineup to win the most. Historically putting people who get on base a lot before 
people who hit extra-base hits a lot seemed to be a good strategy, but there was
no good way to _prove_ that since there are so many other variables at play.

Boiling away all of the other variables, the problem becomes what is the expected
value of a lineup. [Expected Value](https://en.wikipedia.org/wiki/Expected_value)
is a generalization of the weighted average of set of randomly chosen events. For 
this lineup, the Expected Value is the weighted average score the lineup will 
produce.

The EV can be estimated by mass simulation, so running a lineup through a simulated
game of baseball and recording the score, and repeating the process tens of
thousands of times to arrive at an average. Or it can be computed. This project
attempts to do the latter, using dynamic programming.

The next problem is optimization of 9! different permutations of a lineup. Here
this project uses genetic algorithms to breed together lineups to prodce better
ones after repeated generations. This allows us to find good lineups quickly
and show the patterns that produce good lineups while doing it to allow the
user to establish new hypothesis quickly.

## The Technology

* Web Assembly. The EV Function is a large recursive function that explores all
possibilities of game outcomes for a lineup. This project uses Rust and web assembly
to make the speed of this calculation _acceptable_ at around 1 minute per lineup.
* Web Workers. At ~1 minute per lineup would more than 8 months to complete the
entire space, and a generation of 30 would take 30 minutes if everything was done
in serial. Web Workers allow these calculations to be run in parallel without
affecting the runtime of the web application itself. This means a generation will
take as long as the longest lineup calculation instead of the sum of all of them.
* Elm. This helps make writing the user interface and state management systems controlling
the whole application easy and safe. 
* IndexedDB. While the individual workers memoize function results to make the
EV calculation fast, this cache is not shared between workers. IndexedDB is.
* Genetic Algorithms. This process of generating genomes, rating them, and breeding
them according to fitness to explore a space is a good fit for finding maxima
in a very large search space. 

## Demo
[Demo](https://mc706.gitlab.io/genetic-baseball-lineup-optimizer)
