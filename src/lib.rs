use cached::proc_macro::cached;
use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};


// This is like the `main` function, except for JavaScript.
#[wasm_bindgen]
pub fn evaluate_lineup(players:  &JsValue) -> f32 {
   let team: Vec<Player> = players.into_serde().unwrap();
   evaluate_lineup_expected_value(team, 0, GameRecord{inning: 0, score: 0, inning_score: 0, outs: 0, bases: 0})
}

const MAX_INNINGS: u8 = 6;
const MAX_INNING_SCORE: u8 = 5;

#[derive(PartialEq, Eq, Clone, Copy, Hash)]
struct GameRecord {
    inning: u8,
    score: u8,
    inning_score: u8,
    outs: u8,
    bases: u8,
}

#[derive(PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Player {
    strikeout: u16,
    walk: u16,
    hit_out: u16,
    single: u16,
    double: u16,
    triple: u16,
    homerun: u16,
}

fn advance_bases(runners: u8, batter_safe: bool) -> (u8, u8) {
    let advanced = if batter_safe { (runners << 1) + 1 } else {runners << 1};
    (advanced % 8, advanced / 8)
}

fn strikeout(state: GameRecord) -> GameRecord {
    if state.outs + 1 == 3 {
        GameRecord { inning: state.inning + 1, inning_score: 0, outs: 0, bases: 0, ..state }
    } else {
        GameRecord { outs: state.outs + 1 , ..state }
    }
}

fn walk(state: GameRecord) -> GameRecord {
    let (bases, runs) = advance_bases(state.bases, true);
    let score = state.score + runs;
    let inning_score = state.inning_score + runs;
    if inning_score >= MAX_INNING_SCORE {
        GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    } else {
        GameRecord { score, inning_score, bases, ..state}
    }
}

fn hit_out(state: GameRecord) -> GameRecord {
    if state.outs + 1 == 3 {
        return GameRecord { inning: state.inning + 1, inning_score: 0, outs: 0, bases: 0, ..state }
    }
    let (bases, runs) = advance_bases(state.bases, false);
    let score = state.score + runs;
    let inning_score = state.inning_score + runs;
    if inning_score >= MAX_INNING_SCORE {
        GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    } else {
        GameRecord { score, inning_score, bases, outs: state.outs + 1, ..state}
    }
}

fn hit(state: GameRecord) -> GameRecord {
    let (bases, runs) = advance_bases(state.bases, true);
    let score = state.score + runs;
    let inning_score = state.inning_score + runs;
    if inning_score >= MAX_INNING_SCORE {
        GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    } else {
        GameRecord { score, inning_score, bases, ..state}
    }
}

fn double(state: GameRecord) -> GameRecord {
    let (bases, runs) = advance_bases(state.bases, true);
    let mut score = state.score + runs;
    let mut inning_score = state.inning_score + runs;
    if inning_score >= MAX_INNING_SCORE {
        return GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }
    let (bases, runs) = advance_bases(bases, false);
    score += runs;
    inning_score += runs;
    if inning_score >= MAX_INNING_SCORE {
        GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }  else {
        GameRecord { score, inning_score, bases, ..state}
    }
}

fn triple(state: GameRecord) -> GameRecord {
    let (bases, runs)  = advance_bases(state.bases, true);
    let mut score = state.score + runs;
    let mut inning_score = state.inning_score + runs;
    if inning_score >= MAX_INNING_SCORE {
        return GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }
    let (bases, runs)  = advance_bases(bases, false);
    score += runs;
    inning_score += runs;
    if inning_score >= MAX_INNING_SCORE {
        return GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }
    let (bases, runs)  = advance_bases(bases, false);
    score += runs;
    inning_score += runs;
    if inning_score >= MAX_INNING_SCORE {
        GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }  else {
        GameRecord { score, inning_score, bases, ..state}
    }
}

fn homerun(state: GameRecord) -> GameRecord {
    let (bases, runs) = advance_bases(state.bases, true);
    let mut score = state.score + runs;
    let mut inning_score = state.inning_score + runs;
    if inning_score >= MAX_INNING_SCORE {
        return GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }
    let (bases, runs) = advance_bases(bases, false);
    score += runs;
    inning_score += runs;
    if inning_score >= MAX_INNING_SCORE {
        return GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }
    let (bases, runs) = advance_bases(bases, false);
    score += runs;
    inning_score += runs;
    if inning_score >= MAX_INNING_SCORE {
        return GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }
    let (bases, runs) = advance_bases(bases, false);
    score += runs;
    inning_score += runs;
    if inning_score >= MAX_INNING_SCORE {
        GameRecord {inning: state.inning + 1, score, inning_score: 0, outs: 0, bases: 0}
    }  else {
        GameRecord { score, inning_score, bases, ..state}
    }
}


#[cached]
fn evaluate_lineup_expected_value(players: Vec<Player>, player_i: usize, game_state: GameRecord) -> f32 {
    if game_state.inning >= MAX_INNINGS { return game_state.score as f32; }
    let mut expected_value: f32 = 0.0;
    let player = players[player_i];
    let next_player_i = (player_i + 1)  % players.len();
    if player.strikeout > 0  {
        expected_value += (player.strikeout as f32 / 10000.0) * evaluate_lineup_expected_value(players.clone(), next_player_i, strikeout(game_state))
    }
    if player.walk + player.single > 0 {
        expected_value += ((player.walk + player.single) as f32 / 10000.0) * evaluate_lineup_expected_value(players.clone(), next_player_i, walk(game_state))
    }
    if player.hit_out > 0 {
        expected_value += (player.hit_out as f32 / 10000.0) * evaluate_lineup_expected_value(players.clone(), next_player_i, hit_out(game_state))
    }
    if player.double > 0 {
        expected_value += (player.double as f32 / 10000.0) * evaluate_lineup_expected_value(players.clone(), next_player_i, double(game_state))
    }
    if player.triple > 0 {
        expected_value += (player.triple as f32 / 10000.0) * evaluate_lineup_expected_value(players.clone(), next_player_i, triple(game_state))
    }
    if player.homerun > 0 {
        expected_value += (player.homerun as f32 / 10000.0) * evaluate_lineup_expected_value(players.clone(), next_player_i, homerun(game_state))
    }
    expected_value
}
