import localforage from 'localforage';

const rust = import('../pkg/index.js');

const lineupAsKey = lineup => lineup.map(player => Object.values(player).map(n => `${n}`).join('/')).join('|');

addEventListener('message', async e => {
   switch (e.data.type) {
       case "init":
           break;
       case "evaluate":
           let key = lineupAsKey(e.data.lineup);
           let cachedValue = await localforage.getItem(key);
           if (cachedValue) {
               postMessage({value: cachedValue, meta: e.data.meta});
           }
            rust.then(mod => {
                let result = mod.evaluate_lineup(e.data.lineup);
                localforage.setItem(key, result);
                postMessage({value: result, meta: e.data.meta});
            });
            break;
    }
});

