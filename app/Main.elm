port module Main exposing (..)

import Array exposing (Array)
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Form.Input as Input
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.ListGroup as ListGroup
import Bootstrap.Navbar as Navbar
import Bootstrap.Spinner as Spinner
import Bootstrap.Table as Table
import Bootstrap.Text as Text
import Bootstrap.Utilities.Flex as Flex
import Bootstrap.Utilities.Spacing as Spacing
import Browser exposing (Document)
import Chart.HistogramBar as Histo
import Chart.Line as Line
import Color exposing (Color)
import Html exposing (Html, a, br, code, dd, div, dl, dt, h1, hr, p, span, text)
import Html.Attributes exposing (class, href, style)
import Html.Lazy as Lazy
import List.Extra
import Random
import Random.Array
import Random.Extra
import Round
import Set
import Task


type alias Player =
    { name : String
    , strikeout : Int
    , walk : Int
    , hit_out : Int
    , single : Int
    , double : Int
    , triple : Int
    , homerun : Int
    }


emptyPlayer : String -> Player
emptyPlayer name =
    { name = name
    , strikeout = 0
    , walk = 0
    , hit_out = 0
    , single = 0
    , double = 0
    , triple = 0
    , homerun = 0
    }


type alias Config =
    { worker_count : Int
    , generation_size : Int
    , champions : Int
    , fresh_genomes : Float
    , mutation_rate : Float
    }


type ConfigField
    = Workers Int
    | GenerationSize Int
    | Champions Int
    | Fresh Float
    | Mutation Float


type alias Generation =
    { genomes : List (Array Int)
    , fitness : Array Float
    , averageFitness : Maybe Float
    , maxFitness : Maybe Float
    , generation : Int
    }


type Page
    = Landing
    | Lineup
    | Genetics


type alias Model =
    { config : Config
    , players : Array Player
    , generations : List Generation
    , page : Page
    , navState : Navbar.State
    , paused : Bool
    }


type alias ProgramFlags =
    { players : List Player
    }


type BsMsg
    = NavbarMsg Navbar.State


type Field
    = Name
    | Strikeout
    | Walk
    | HitOut
    | Single
    | Double
    | Triple
    | HomeRun


type Msg
    = NoOp
    | GoTo Page
    | BsMsg BsMsg
    | UpdatePlayerField Int Field String
    | ProcessGeneration
    | GenerateGeneration (List (Array Int))
    | ApplyMutations (List (List Int))
    | EvaluationCompleted WorkerResponse
    | AddPlayer
    | ShareLineup
    | TogglePause Bool
    | ChangeConfig ConfigField


type alias WorkerResponse =
    { index : Int
    , value : Float
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GoTo page ->
            let
                cmd =
                    case page of
                        Genetics ->
                            Cmd.batch [ send ProcessGeneration, saveLineup <| Array.toList model.players ]

                        _ ->
                            Cmd.none
            in
            ( { model | page = page }, cmd )

        BsMsg bsMsg ->
            case bsMsg of
                NavbarMsg state ->
                    ( { model | navState = state }, Cmd.none )

        AddPlayer ->
            ( { model | players = Array.push (emptyPlayer "") model.players }, Cmd.none )

        ShareLineup ->
            ( model, copyShareLink <| Array.toList model.players )

        UpdatePlayerField index field value ->
            let
                oldPlayer =
                    Array.get index model.players |> Maybe.withDefault (emptyPlayer " ")

                cleanValue : String -> Maybe Int
                cleanValue =
                    String.toFloat >> Maybe.map ((*) 100.0) >> Maybe.map round

                newPlayer =
                    case field of
                        Name ->
                            { oldPlayer | name = value }

                        Strikeout ->
                            { oldPlayer | strikeout = Maybe.withDefault 0 <| cleanValue value }

                        Walk ->
                            { oldPlayer | walk = Maybe.withDefault 0 <| cleanValue value }

                        HitOut ->
                            { oldPlayer | hit_out = Maybe.withDefault 0 <| cleanValue value }

                        Single ->
                            { oldPlayer | single = Maybe.withDefault 0 <| cleanValue value }

                        Double ->
                            { oldPlayer | double = Maybe.withDefault 0 <| cleanValue value }

                        Triple ->
                            { oldPlayer | triple = Maybe.withDefault 0 <| cleanValue value }

                        HomeRun ->
                            { oldPlayer | homerun = Maybe.withDefault 0 <| cleanValue value }

                newPlayers =
                    Array.set index newPlayer model.players
            in
            ( { model | players = newPlayers }, Cmd.none )

        ProcessGeneration ->
            if model.paused then
                ( model, Cmd.none )

            else
                ( model
                , Random.generate GenerateGeneration (Random.list model.config.generation_size (Random.Array.shuffle (Array.initialize (Array.length model.players) identity)))
                )

        TogglePause paused ->
            if paused then
                ( { model | paused = paused }, Cmd.none )

            else if model.generations |> List.head |> Maybe.map generationIsComplete |> Maybe.withDefault False then
                ( { model | paused = paused }, send ProcessGeneration )

            else
                ( { model | paused = paused }, Cmd.none )

        GenerateGeneration genomes ->
            case model.generations of
                [] ->
                    let
                        firstGeneration =
                            { genomes = genomes
                            , fitness = Array.initialize (List.length genomes) (always -1)
                            , averageFitness = Nothing
                            , maxFitness = Nothing
                            , generation = 0
                            }
                    in
                    ( { model | generations = [ firstGeneration ] }, evaluateGeneration (serializePlayers model.players firstGeneration) )

                lastGen :: _ ->
                    let
                        champions =
                            generationOrdered lastGen
                                |> List.map Tuple.first
                                |> List.take model.config.champions

                        breedPoolSize =
                            model.config.generation_size - model.config.champions - (toFloat model.config.generation_size * model.config.fresh_genomes |> round)

                        bredGenomes =
                            genomes
                                |> List.take breedPoolSize
                                |> breedGenes lastGen
                                |> (++) champions
                                |> List.Extra.uniqueBy (Array.toList >> List.map String.fromInt >> String.join "-")

                        freshGenomes =
                            genomes
                                |> List.reverse
                                |> List.take (model.config.generation_size - List.length bredGenomes)

                        nextGenomes =
                            bredGenomes ++ freshGenomes

                        nextGeneration =
                            { genomes = nextGenomes
                            , fitness = Array.initialize (List.length genomes) (always -1)
                            , averageFitness = Nothing
                            , maxFitness = Nothing
                            , generation = lastGen.generation + 1
                            }
                    in
                    ( { model | generations = nextGeneration :: model.generations }
                    , Random.generate ApplyMutations (mutationGenerator model)
                    )

        ApplyMutations mutations ->
            case model.generations of
                [] ->
                    ( model, Cmd.none )

                latestGen :: history ->
                    let
                        offsetMutations =
                            List.repeat model.config.champions [] ++ mutations

                        mutatedGeneration =
                            { latestGen | genomes = latestGen.genomes |> applyMutations offsetMutations }
                    in
                    ( { model | generations = mutatedGeneration :: history }, evaluateGeneration (serializePlayers model.players mutatedGeneration) )

        EvaluationCompleted { index, value } ->
            case model.generations of
                [] ->
                    ( model, Cmd.none )

                lastGen :: history ->
                    let
                        newFitness =
                            Array.set index value lastGen.fitness

                        asList =
                            Array.toList newFitness

                        newMax =
                            asList |> List.maximum

                        onlyValues =
                            asList |> List.filter ((/=) -1.0)

                        newAvg =
                            onlyValues |> List.sum |> (\total -> total / toFloat (List.length onlyValues)) |> Just

                        updatedGeneration =
                            { lastGen | fitness = newFitness, maxFitness = newMax, averageFitness = newAvg }

                        cmd =
                            if generationIsComplete updatedGeneration then
                                send ProcessGeneration

                            else
                                Cmd.none
                    in
                    ( { model | generations = updatedGeneration :: history }, cmd )

        ChangeConfig configField ->
            case configField of
                Workers count ->
                    let
                        diff =
                            count - model.config.worker_count

                        cmd =
                            if diff > 0 then
                                bootWorkers diff

                            else
                                shutdownWorkers <| abs diff

                        config =
                            model.config

                        newConfig =
                            { config | worker_count = count }
                    in
                    ( { model | config = newConfig }, cmd )

                GenerationSize count ->
                    let
                        config =
                            model.config

                        newConfig =
                            { config | generation_size = count }
                    in
                    ( { model | config = newConfig }, Cmd.none )

                Champions count ->
                    let
                        config =
                            model.config

                        newConfig =
                            { config | champions = count }
                    in
                    ( { model | config = newConfig }, Cmd.none )

                Fresh percent ->
                    let
                        config =
                            model.config

                        newConfig =
                            { config | fresh_genomes = percent }
                    in
                    ( { model | config = newConfig }, Cmd.none )

                Mutation percent ->
                    let
                        config =
                            model.config

                        newConfig =
                            { config | mutation_rate = percent }
                    in
                    ( { model | config = newConfig }, Cmd.none )


mutationGenerator : Model -> Random.Generator (List (List Int))
mutationGenerator model =
    Random.list model.config.generation_size <|
        Random.Extra.frequency
            ( model.config.mutation_rate
            , Random.Extra.rangeLengthList 0 (Array.length model.players) <|
                Random.int 0 (Array.length model.players)
            )
            [ ( 1.0 - model.config.mutation_rate, Random.list 0 (Random.int 0 0) ) ]


toTuple : List a -> Maybe ( a, a )
toTuple list =
    case list of
        [] ->
            Nothing

        [ a ] ->
            Just ( a, a )

        a :: b :: _ ->
            Just ( a, b )


applyMutations : List (List Int) -> List (Array Int) -> List (Array Int)
applyMutations mutations population =
    let
        swapPairs =
            mutations
                |> List.map (List.Extra.groupsOf 2)
                |> List.map (List.filterMap toTuple)

        tupleSwap ( a, b ) =
            swap a b

        mutate : List ( Int, Int ) -> Array Int -> Array Int
        mutate operations genome =
            operations
                |> List.foldl tupleSwap genome
    in
    population
        |> List.map2 mutate swapPairs


swap : Int -> Int -> Array Int -> Array Int
swap src dest array =
    case ( Array.get src array, Array.get dest array ) of
        ( Just srcValue, Just destValue ) ->
            array
                |> Array.set dest srcValue
                |> Array.set src destValue

        _ ->
            array


find : Int -> Array Int -> Int
find needle haystack =
    Array.toList haystack
        |> List.indexedMap Tuple.pair
        |> List.filter (Tuple.second >> (==) needle)
        |> List.head
        |> Maybe.map Tuple.first
        |> Maybe.withDefault -1


infuseTrait : ( Int, Int ) -> Array Int -> Array Int
infuseTrait ( index, value ) base =
    base
        |> swap index (find value base)


findCommonIndicies : Array Int -> Array Int -> List ( Int, Int )
findCommonIndicies array_a array_b =
    List.map2 Tuple.pair (Array.toList array_a) (Array.toList array_b)
        |> List.indexedMap Tuple.pair
        |> List.filter (\( _, ( a, b ) ) -> a == b)
        |> List.map (\( i, ( a, _ ) ) -> ( i, a ))


breedGenes : Generation -> List (Array Int) -> List (Array Int)
breedGenes generation stock =
    let
        topFit : Int
        topFit =
            round (0.2 * toFloat (List.length generation.genomes))
                |> min 4

        mostFitParents : Array ( Array Int, Array Int )
        mostFitParents =
            generationOrdered generation
                |> List.map Tuple.first
                |> List.take topFit
                |> List.Extra.uniquePairs
                |> List.reverse
                |> Array.fromList

        breedParents : Int -> Array Int -> Array Int
        breedParents i base =
            let
                ( parentA, parentB ) =
                    Array.get (modBy (Array.length mostFitParents) i) mostFitParents |> Maybe.withDefault ( Array.empty, Array.empty )

                passedTraits =
                    findCommonIndicies parentA parentB
            in
            List.foldl infuseTrait base passedTraits
    in
    stock
        |> List.indexedMap breedParents


generationIsComplete : Generation -> Bool
generationIsComplete gen =
    Array.toList gen.fitness |> List.all ((/=) -1.0)


serializePlayers : Array Player -> Generation -> List (Array Player)
serializePlayers playerArray generation =
    let
        mapPlayer : Int -> Player
        mapPlayer i =
            Array.get i playerArray |> Maybe.withDefault (emptyPlayer "")

        buildPlayerLineup : Array Int -> Array Player
        buildPlayerLineup genome =
            Array.map mapPlayer genome
    in
    generation.genomes
        |> List.map buildPlayerLineup


view : Model -> Document Msg
view model =
    { title = "Baseball Lineup Genetic Optimizer"
    , body = [ viewApp model ]
    }


viewApp : Model -> Html Msg
viewApp model =
    div []
        [ viewHeader model
        , viewContent model
        ]


viewHeader : Model -> Html Msg
viewHeader model =
    Navbar.config (BsMsg << NavbarMsg)
        |> Navbar.brand [ href "#" ] [ text "Baseball Lineup Genetic Optimizer" ]
        |> Navbar.dark
        |> Navbar.view model.navState


viewContent : Model -> Html Msg
viewContent model =
    case model.page of
        Landing ->
            viewLanding

        Lineup ->
            viewLineup model

        Genetics ->
            viewGenetics model


viewLanding : Html Msg
viewLanding =
    Grid.container [ Spacing.pt3 ]
        [ div [ class "jumbotron" ]
            [ h1 [ class "display-4" ] [ text "Welcome to the Baseball Lineup Genetic Optimizer" ]
            , p [ class "lead" ] [ text "This project is an exploration of using genetic algorithms to optimize baseball lineups." ]
            , hr [ Spacing.my4 ] []
            , p []
                [ text """
        The problem of optimizing baseball lineups is a complex one. No to players are alike. There are tons of variables, and millions of combinations. 
        Baseball itself is a game with so many other variables, so it is hard to control for things like errors and players choice on the performance of a particular lineup. 
        This project seeks to explore this problems space while utilizing some cool technologies along the way."""
                ]
            , p []
                [ text """
        The first part of the problem is measuring success. We need some metric that is solely dependent on the players and the order of the lineup. Isolating
        and removing uncontrollable variables leaves us playing a one sided game, where the ultimate score of the lineup is purely based on the statistics of
        the players in the lineup and the given order. Previous iterations of this tool used random number generators to simulate tens of thousands of games for a given
        lineup and averaged the results to create a score. This required a lot of computational power and took a lot of time to get a value that had a pretty high
        variance from run to run.""" ]
            , p []
                [ text """This iteration calculates the """
                , a [ href "https://en.wikipedia.org/wiki/Expected_value" ] [ text "Expected Value" ]
                , text """ of a lineup, by evaluating all possible games for a given lineup and summing the score of the game times the probability of game being played. 
        This results in a deterministic value for every lineup that is the score on average of playing this one-sided game of baseball. While this is also computationally 
        expensive, there are some algorithmic efficiencies that can cut down on the search space, and the computation can be paired with some powerful web technologies to allow
        it to run in your browser in a reasonable amount of time. This project uses Rust, Web Assembly and Web workers to speed up and parallelize this evaluation process."""
                ]
            , p []
                [ text """
        The next hard problem in dealing with lineups is permutations. For a 9 player team, there are 9! (362,880) permutations. Given each permutation
        has a score that is non-trivial to compute, searching the entire space for a global maxima is computationally very expensive. This is where this tool uses
        genetic algorithms to narrow in on an optimal solution quickly. At its core, each line can be seen as a genome, and each genome can be scored using the
        expected value calculation as the fitness function. Given a very small handful off lineups, the algorithm can "breed" the most fit lineups together, taking
        "traits" (key player/position pairs) into the next generation. This approach allows us to use statistics to continually guess and test the best traits that form
        a optimized lineup, and should quickly result in an optimized lineup after a few dozen generations."""
                ]
            , p [ class "lead" ] [ Button.button [ Button.primary, Button.onClick (GoTo Lineup) ] [ text "Get Started" ] ]
            ]
        ]


intToFloatStr : Int -> String
intToFloatStr int =
    String.fromFloat (toFloat int / 100.0)


viewLineup : Model -> Html Msg
viewLineup model =
    let
        numberCell : Field -> (Player -> Int) -> Int -> Player -> Table.Cell Msg
        numberCell field getter i player =
            Table.td []
                [ Input.number
                    [ Input.onInput (UpdatePlayerField i field)
                    , Input.value <| intToFloatStr <| getter player
                    , Input.attrs
                        [ Html.Attributes.min "0"
                        , Html.Attributes.max "100"
                        , Html.Attributes.step "0.01"
                        ]
                    ]
                ]

        renderRow : Int -> Player -> Table.Row Msg
        renderRow i player =
            Table.tr []
                [ Table.td []
                    [ Input.text
                        [ Input.onInput (UpdatePlayerField i Name)
                        , Input.value player.name
                        ]
                    ]
                , numberCell Strikeout .strikeout i player
                , numberCell Walk .walk i player
                , numberCell HitOut .hit_out i player
                , numberCell Single .single i player
                , numberCell Double .double i player
                , numberCell Triple .triple i player
                , numberCell HomeRun .homerun i player
                ]
    in
    Grid.container [ Spacing.pt3 ]
        [ h1 [] [ text "Input your Players" ]
        , p [] [ text """
        To get started, first we need some information about your players statistically. Note none of this information is sent anywhere (this tool works entirely in
        your browser and sends no data anywhere). Make sure every player has at at least some statistics, or else the EV function throws an expected value of 0, which
        multiplies through and invalidates all of the data. Each of these statistics represents the % of time that a given outcome happens when that player is at bat.
        """ ]
        , br [] []
        , Table.table
            { options = [ Table.bordered, Table.small ]
            , thead =
                Table.simpleThead
                    [ Table.th [] [ text "Name" ]
                    , Table.th [] [ text "Strikeout %" ]
                    , Table.th [] [ text "Walk %" ]
                    , Table.th [] [ text "Hit Out %" ]
                    , Table.th [] [ text "Single %" ]
                    , Table.th [] [ text "Double %" ]
                    , Table.th [] [ text "Triple %" ]
                    , Table.th [] [ text "HomeRun %" ]
                    ]
            , tbody = Table.tbody [] (model.players |> Array.indexedMap renderRow |> Array.toList)
            }
        , Button.button [ Button.small, Button.outlinePrimary, Button.onClick AddPlayer ] [ text "Add Player" ]
        , div [ Flex.block, Flex.justifyCenter ]
            [ Button.button [ Button.primary, Button.onClick (GoTo Genetics), Button.disabled <| not <| canOptimizeLineup model ] [ text "Optimize Lineup" ] ]
        ]


canOptimizeLineup : Model -> Bool
canOptimizeLineup model =
    let
        playerIsValid : Player -> Bool
        playerIsValid player =
            player.strikeout + player.walk + player.hit_out + player.single + player.double + player.triple + player.homerun > 0
    in
    model.players
        |> Array.toList
        |> List.all playerIsValid


viewGenetics : Model -> Html Msg
viewGenetics model =
    Grid.containerFluid [ Spacing.pt3 ]
        [ Grid.simpleRow
            [ Grid.col [ Col.xs3 ]
                [ viewChampion model
                , viewConfigCard model
                , viewButtons model
                ]
            , Grid.col [ Col.xs9 ]
                [ viewCharts model
                , viewGenerations model
                ]
            ]
        ]


viewConfigCard : Model -> Html Msg
viewConfigCard model =
    Card.config []
        |> Card.headerH6 [] [ text "Genetics Config" ]
        |> Card.listGroup
            [ ListGroup.li [ ListGroup.attrs [ Flex.block, Flex.justifyBetween, Flex.alignItemsCenter ] ]
                [ span [] [ text "Workers:" ]
                , Input.number
                    [ Input.onInput (String.toInt >> Maybe.withDefault model.config.worker_count >> Workers >> ChangeConfig)
                    , Input.value <| String.fromInt <| model.config.worker_count
                    , Input.small
                    , Input.attrs
                        [ Html.Attributes.min "1"
                        , Html.Attributes.max "100"
                        , Html.Attributes.step "1"
                        , style "width" "auto"
                        ]
                    ]
                ]
            , ListGroup.li [ ListGroup.attrs [ Flex.block, Flex.justifyBetween, Flex.alignItemsCenter ] ]
                [ span [] [ text "Generation Size:" ]
                , Input.number
                    [ Input.onInput (String.toInt >> Maybe.withDefault model.config.generation_size >> GenerationSize >> ChangeConfig)
                    , Input.value <| String.fromInt <| model.config.generation_size
                    , Input.small
                    , Input.attrs
                        [ Html.Attributes.min "10"
                        , Html.Attributes.max "100"
                        , Html.Attributes.step "1"
                        , style "width" "auto"
                        ]
                    ]
                ]
            , ListGroup.li [ ListGroup.attrs [ Flex.block, Flex.justifyBetween, Flex.alignItemsCenter ] ]
                [ span [] [ text "Champions:" ]
                , Input.number
                    [ Input.onInput (String.toInt >> Maybe.withDefault model.config.champions >> Champions >> ChangeConfig)
                    , Input.value <| String.fromInt <| model.config.champions
                    , Input.small
                    , Input.attrs
                        [ Html.Attributes.min "1"
                        , Html.Attributes.max "5"
                        , Html.Attributes.step "1"
                        , style "width" "auto"
                        ]
                    ]
                ]
            , ListGroup.li [ ListGroup.attrs [ Flex.block, Flex.justifyBetween, Flex.alignItemsCenter ] ]
                [ span [] [ text "Fresh Gene Percent:" ]
                , Input.number
                    [ Input.onInput (String.toFloat >> Maybe.withDefault model.config.fresh_genomes >> Fresh >> ChangeConfig)
                    , Input.value <| String.fromFloat <| model.config.fresh_genomes
                    , Input.small
                    , Input.attrs
                        [ Html.Attributes.min ".1"
                        , Html.Attributes.max ".9"
                        , Html.Attributes.step ".05"
                        , style "width" "auto"
                        ]
                    ]
                ]
            , ListGroup.li [ ListGroup.attrs [ Flex.block, Flex.justifyBetween, Flex.alignItemsCenter ] ]
                [ span [] [ text "Mutation Rate:" ]
                , Input.number
                    [ Input.onInput (String.toFloat >> Maybe.withDefault model.config.mutation_rate >> Mutation >> ChangeConfig)
                    , Input.value <| String.fromFloat <| model.config.mutation_rate
                    , Input.small
                    , Input.attrs
                        [ Html.Attributes.min ".00"
                        , Html.Attributes.max ".4"
                        , Html.Attributes.step ".01"
                        , style "width" "auto"
                        ]
                    ]
                ]
            ]
        |> Card.view


viewButtons : Model -> Html Msg
viewButtons model =
    div [ Spacing.mt3, Flex.block, Flex.row, Flex.justifyBetween ]
        [ viewShareLink
        , viewPlayPause model.paused
        ]


viewShareLink : Html Msg
viewShareLink =
    Button.button [ Button.primary, Button.onClick ShareLineup ] [ text "Share Lineup" ]


viewPlayPause : Bool -> Html Msg
viewPlayPause paused =
    if paused then
        Button.button [ Button.warning, Button.onClick (TogglePause (not paused)) ] [ text "Play" ]

    else
        Button.button [ Button.success, Button.onClick (TogglePause (not paused)) ] [ text "Pause" ]


spinner : Html msg
spinner =
    Spinner.spinner [ Spinner.small, Spinner.color Text.dark ] [ Spinner.srMessage "Loading" ]


showColorGene : Array Int -> Html msg
showColorGene gene =
    div [ Flex.block, Flex.row ]
        (Array.map showPlayerColorBlock gene |> Array.toList)


showPlayerColorBlock : Int -> Html msg
showPlayerColorBlock i =
    div [ style "width" "16px", style "height" "16px", style "background-color" (Color.toCssString (Array.get i colorWheel |> Maybe.withDefault Color.white)) ] []


showFitnessNum : Float -> Html msg
showFitnessNum fitness =
    if fitness == -1 then
        spinner

    else
        code [] [ text <| Round.round 4 fitness ]


viewGenerationCard : Generation -> ( String, Card.Config msg )
viewGenerationCard generation =
    let
        key =
            if generationIsComplete generation then
                "generation_" ++ String.fromInt generation.generation

            else
                "remaining_" ++ String.fromInt (generation.fitness |> Array.toList |> List.filter ((==) -1.0) |> List.length)

        showGenome : ( Array Int, Float ) -> ListGroup.Item msg
        showGenome ( genome, fitness ) =
            ListGroup.li [ ListGroup.attrs [ Flex.block, Flex.justifyBetween, Flex.alignItemsCenter, Spacing.px1, Spacing.py0 ] ]
                [ showColorGene genome
                , div [ style "font-size" ".9em" ] [ showFitnessNum fitness ]
                ]
    in
    ( key
    , Card.config [ Card.attrs [ style "max-width" "275px", style "min-width" "275px" ] ]
        |> Card.headerH6 [] [ text <| "Gen" ++ String.fromInt generation.generation ]
        |> Card.block [ Block.attrs [ Spacing.p1 ] ]
            [ Block.custom
                (dl [ class "row", Spacing.m0 ]
                    [ dt [ class "col-6" ] [ text "Max Fitness" ]
                    , dd [ class "col-6 text-right" ] [ generation.maxFitness |> Maybe.withDefault -1 |> showFitnessNum ]
                    , dt [ class "col-6" ] [ text "Avg Fitness" ]
                    , dd [ class "col-6 text-right" ] [ generation.averageFitness |> Maybe.withDefault -1 |> showFitnessNum ]
                    ]
                )
            ]
        |> Card.listGroup (generationOrdered generation |> List.map showGenome)
    )


viewGenerations : Model -> Html Msg
viewGenerations model =
    div [ style "overflow-x" "scroll" ]
        [ Card.keyedDeck (List.map viewGenerationCard model.generations) ]


viewCharts : Model -> Html Msg
viewCharts model =
    let
        allFitness =
            case model.generations of
                _ :: history ->
                    history
                        |> List.concatMap generationOrdered
                        |> List.Extra.uniqueBy (Tuple.first >> Array.map String.fromInt >> Array.toList >> String.join "-")
                        |> List.map Tuple.second

                _ ->
                    []

        generationalFitness =
            case model.generations of
                _ :: history ->
                    history
                        |> List.map (\gen -> ( gen.maxFitness |> Maybe.withDefault 0, gen.averageFitness |> Maybe.withDefault 0 ))
                        |> List.reverse

                _ ->
                    []
    in
    Grid.row [ Row.attrs [ Spacing.mb2 ] ]
        [ Grid.col []
            [ Lazy.lazy viewFitnessHistogram allFitness ]
        , Grid.col []
            [ Lazy.lazy viewFitnessOverTime generationalFitness ]
        ]


viewFitnessHistogram : List Float -> Html Msg
viewFitnessHistogram allFitness =
    let
        minFitness =
            List.minimum allFitness |> Maybe.withDefault 0.0

        maxFitness =
            List.maximum allFitness |> Maybe.withDefault 100.0

        fitnessWithoutChampionRepeats =
            if List.isEmpty allFitness then
                []

            else
                maxFitness :: List.filter ((/=) maxFitness) allFitness

        buckets =
            25

        stepSize =
            (maxFitness - minFitness) / toFloat buckets

        steps =
            List.range 0 buckets |> List.map (\i -> minFitness + stepSize * toFloat i)
    in
    Histo.init
        { margin = { top = 10, left = 25, right = 10, bottom = 20 }
        , width = 500
        , height = 250
        }
        |> Histo.withDomain ( minFitness - 1, maxFitness + 1 )
        |> Histo.withTitle "Overall Expected Value Distribution"
        |> Histo.render ( fitnessWithoutChampionRepeats, Histo.dataAccessor steps identity )


type alias DataLinear =
    { x : Float, y : Float, groupLabel : String }


viewFitnessOverTime : List ( Float, Float ) -> Html Msg
viewFitnessOverTime data =
    let
        toLinePoints : Int -> ( Float, Float ) -> List DataLinear
        toLinePoints i ( max, avg ) =
            [ { groupLabel = "Max", x = toFloat i, y = max }
            , { groupLabel = "Avg", x = toFloat i, y = avg }
            ]

        lineData : List DataLinear
        lineData =
            data
                |> List.indexedMap toLinePoints
                |> List.concat

        minY =
            lineData |> List.Extra.minimumBy .y |> Maybe.map .y |> Maybe.map ((+) -1.0) |> Maybe.withDefault 0.0

        maxY =
            lineData |> List.Extra.maximumBy .y |> Maybe.map .y |> Maybe.map ((+) 1.0) |> Maybe.withDefault 100.0

        xAxisTicks : List Float
        xAxisTicks =
            lineData
                |> List.map .x
                |> Set.fromList
                |> Set.toList
                |> List.sort

        accessorLinear : Line.Accessor DataLinear
        accessorLinear =
            Line.linear (Line.AccessorLinear (.groupLabel >> Just) .x .y)
    in
    Line.init
        { margin = { top = 10, left = 30, right = 10, bottom = 20 }
        , width = 500
        , height = 250
        }
        |> Line.withTitle "Fitness Over Time"
        |> Line.withXAxisTicks xAxisTicks
        |> Line.withYDomain ( minY, maxY )
        |> Line.withColorPalette (Array.toList colorWheel)
        |> Line.render ( lineData, accessorLinear )


generationOrdered : Generation -> List ( Array Int, Float )
generationOrdered generation =
    List.map2 Tuple.pair generation.genomes (Array.toList generation.fitness)
        |> List.sortBy Tuple.second
        |> List.reverse


generationChampion : Config -> Generation -> Array Int
generationChampion config generation =
    generationOrdered generation
        |> List.head
        |> Maybe.map Tuple.first
        |> Maybe.withDefault (Array.initialize config.generation_size identity)


viewChampion : Model -> Html Msg
viewChampion model =
    let
        ( order, championLineup ) =
            case model.generations of
                _ :: gen :: _ ->
                    let
                        o =
                            generationChampion model.config gen
                    in
                    ( o
                    , o
                        |> Array.map (\i -> Array.get i model.players |> Maybe.withDefault (emptyPlayer ""))
                    )

                _ ->
                    ( Array.initialize (Array.length model.players) identity, model.players )

        renderRow : ( Int, Player ) -> Table.Row msg
        renderRow ( i, player ) =
            Table.tr []
                ([ showPlayerColorBlock i
                 , text <| player.name
                 , text <| intToFloatStr <| player.strikeout
                 , text <| intToFloatStr <| player.walk
                 , text <| intToFloatStr <| player.hit_out
                 , text <| intToFloatStr <| player.single
                 , text <| intToFloatStr <| player.double
                 , text <| intToFloatStr <| player.triple
                 , text <| intToFloatStr <| player.homerun
                 ]
                    |> List.map (\g -> Table.td [] [ g ])
                )
    in
    div []
        [ Table.table
            { options = [ Table.bordered, Table.small ]
            , thead =
                Table.simpleThead
                    [ Table.th [] []
                    , Table.th [] [ text "Name" ]
                    , Table.th [] [ text "X" ]
                    , Table.th [] [ text "W" ]
                    , Table.th [] [ text "O" ]
                    , Table.th [] [ text "1" ]
                    , Table.th [] [ text "2" ]
                    , Table.th [] [ text "3" ]
                    , Table.th [] [ text "HR" ]
                    ]
            , tbody = Table.tbody [] (List.map2 Tuple.pair (Array.toList order) (Array.toList championLineup) |> List.map renderRow)
            }
        ]


init : ProgramFlags -> ( Model, Cmd Msg )
init flags =
    let
        ( navState, navCmd ) =
            Navbar.initialState (BsMsg << NavbarMsg)

        defaultConfig : Config
        defaultConfig =
            { worker_count = 20
            , generation_size = 20
            , champions = 1
            , fresh_genomes = 0.4
            , mutation_rate = 0.05
            }
    in
    case flags.players of
        [] ->
            ( { page = Landing
              , config = defaultConfig
              , players = [ "A", "B", "C", "D", "E", "F", "G", "H", "I" ] |> List.map emptyPlayer |> Array.fromList
              , generations = []
              , navState = navState
              , paused = False
              }
            , Cmd.batch [ navCmd, bootWorkers defaultConfig.worker_count ]
            )

        players ->
            ( { page = Landing
              , config = defaultConfig
              , players = Array.fromList players
              , generations = []
              , navState = navState
              , paused = False
              }
            , Cmd.batch [ navCmd, bootWorkers defaultConfig.worker_count ]
            )


colorWheel =
    Array.fromList
        [ Color.red
        , Color.green
        , Color.orange
        , Color.blue
        , Color.yellow
        , Color.purple
        , Color.brown
        , Color.lightRed
        , Color.lightGreen
        , Color.lightOrange
        , Color.lightBlue
        , Color.lightYellow
        , Color.lightPurple
        , Color.lightBrown
        ]


send : msg -> Cmd msg
send msg =
    Task.succeed msg
        |> Task.perform identity


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ expectedValueCompleted <| EvaluationCompleted ]


port bootWorkers : Int -> Cmd msg


port expectedValueCompleted : (WorkerResponse -> msg) -> Sub msg


port evaluateGeneration : List (Array Player) -> Cmd msg


port saveLineup : List Player -> Cmd msg


port copyShareLink : List Player -> Cmd msg


port shutdownWorkers : Int -> Cmd msg


main : Program ProgramFlags Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }
