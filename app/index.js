import Worker from 'worker-loader!./worker';

import {Elm} from './Main.elm';

const getFlags = () => {
    let share = window.location.search.replace('?share=', '');
    if (share) {
        return {players: Object.values(JSON.parse(atob(share)))}
    }
    let players = localStorage.getItem('players');
    if (players) {
        return {players: Object.values(JSON.parse(players))}
    }
    return {players: []};
}

const app = Elm.Main.init({flags: getFlags()});

app.workers = [];
app.work_queue = [];

const handleWorker = (msg) => {
    let {value, meta} = msg.data;
    app.ports.expectedValueCompleted.send({value, index: meta.index});
    if (app.work_queue.length > 0) {
        let workMsg = app.work_queue.pop();
        workMsg.worker_index = meta.worker_index;
        app.workers[meta.worker_index].postMessage(workMsg);
    }
};

app.ports.saveLineup.subscribe(players => {
    let playersObject = Object.fromEntries(players.map((player, i) => [i, player]));
    localStorage.setItem('players', JSON.stringify(playersObject));
});

const copyToClipboard = str => {
    console.log('str', str);
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};

app.ports.copyShareLink.subscribe(players => {
    let playersObject = Object.fromEntries(players.map((player, i) => [i, player]));
    let shareData = btoa(JSON.stringify(playersObject));
    let shareLink = `${window.location.origin}${window.location.pathname}?share=${shareData}`;
    copyToClipboard(shareLink);
});

app.ports.bootWorkers.subscribe(n => {
    [...Array(n).keys()].forEach(() => {
        let worker = new Worker();
        worker.postMessage({type: 'init'});
        worker.onmessage = handleWorker;
        app.workers.push(worker);
    });
});

app.ports.shutdownWorkers.subscribe(n => {
    [...Array(n).keys()].forEach(() => {
        let worker = app.workers.pop();
        worker.terminate();
    });
})

app.ports.evaluateGeneration.subscribe(generation => {
    generation.forEach((lineup, i) => {
        let cleanedLineup = lineup.map(({strikeout, walk, hit_out, single, double, triple, homerun}) => ({
            strikeout,
            walk,
            hit_out,
            single,
            double,
            triple,
            homerun
        }));
        app.work_queue.unshift({
            type: 'evaluate',
            lineup: cleanedLineup,
            meta: {index: i}
        });
    });
    app.workers.forEach((worker, n) => {
        if (app.work_queue.length > 0) {
            let msg = app.work_queue.pop();
            msg.meta.worker_index = n;
            worker.postMessage(msg);
        }
    })
});
